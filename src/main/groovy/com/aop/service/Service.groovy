package com.aop.service

import org.springframework.beans.factory.annotation.Autowired
import com.aop.service.ServiceRequestingRest

@org.springframework.stereotype.Service
class Service {

    @Autowired
    ServiceRequestingRest serviceThrowingExc

    void serveAndFail() {
        Thread.sleep(500);

        serviceThrowingExc.serveAndThrow();

        System.out.println("500 elapsed")
    }

    String serveWithExceptionCatching() {
        try {
            String ret = serviceThrowingExc.MakeRequest();

            ret
        } catch (Exception ex) {
           System.out.println("Exception has been handled")
           "FAIL"
        }
    }
}
