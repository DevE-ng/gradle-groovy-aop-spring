package com.aop.service

import com.aop.annotations.FailOrSuccessOperationRegister
import com.aop.metrics.MetricTypes

@org.springframework.stereotype.Service
class ServiceRequestingRest {


    @FailOrSuccessOperationRegister(metricName= MetricTypes.OblikRestAccess)
    String MakeRequest() {
        URL link = new URL("http", "localhost",8080 , "/serve/metrics")

        try {
            //new URL(url).openConnection().getInputStream()
            Integer intV = Random.newInstance().nextInt(10)
            System.out.println("Rand: ${intV}")
            if (intV > 2) throw new ArithmeticException()
            System.out.println("OK")
            return  "OK <a href='${link}' traget='new'>Metrics</a>"
        } catch (Exception e) {
            System.out.println("REST EXCEPTION: ${e.getClass().getCanonicalName()} , ${e.getMessage()}")
        }
        return "FAIL <a href='${link}' target='_blank'>Metrics</a>"




    }


}
