package com.aop.annotations

import com.aop.metrics.MetricTypes

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

@Retention(RetentionPolicy.RUNTIME)
@interface FailOrSuccessOperationRegister {
    MetricTypes metricName();
}