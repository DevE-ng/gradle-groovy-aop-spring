package com.aop.controller

import com.aop.service.Service
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import com.aop.metrics.MetricTypes;
import com.aop.metrics.MetricsCollection;

@RestController
@RequestMapping('serve')
public class GetTest {

    @Autowired
    Service service

    @GetMapping('fail')
    String  serverAndFail(){
        service.serveAndFail()

        "OK"
    }

    @GetMapping('metrics')
    String getMetrics(){

        String ret = "[<BR/>"
        MetricTypes.values().each {
            ret += "{ '${it}' : {"
            ret += " 'count' : '  ${MetricsCollection.FallsAmount(it.toString())}' ,"
            ret += " 'timeframe' : '  ${MetricsCollection.FailingDuration(it.toString())} ms' "
            ret += "}}<BR/>"
        }
        ret += "]"
        ret
    }

    @GetMapping('success')
    String  callExteralRestOfOblik(){
        String ret = service.serveWithExceptionCatching()

        String metrics = getMetrics()
        "${ret} , <BR/> <BR/> ${metrics}"
    }
}
