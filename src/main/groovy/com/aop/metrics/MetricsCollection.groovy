package com.aop.metrics

import java.util.concurrent.ConcurrentHashMap

class MetricsCollection {

    private static ConcurrentHashMap<String,MetricStruct> metrics = new ConcurrentHashMap();

    static
    {
            MetricTypes.values().each {
                //metrics[it] = 0;
                MetricStruct struct = new MetricStruct()
                struct.lastTimeFrameExceptionDuration = 0
                struct.count = 0
                metrics.put(it.toString(), struct)

            }
    }

    static void IncreaseFalls(String metricName){
        metrics.get(metricName).count += 1;
    }

    static void ZeroOutFall(String metricName){
        metrics.get(metricName).count = 0
    }

    static void SetFailingDuration(String metricName,long dur){
        metrics.get(metricName).lastTimeFrameExceptionDuration = dur
    }

    static Integer FallsAmount(String metricName){
        metrics.get(metricName).count
    }

    static long FailingDuration(String metricName){
        metrics.get(metricName).lastTimeFrameExceptionDuration
    }

}
