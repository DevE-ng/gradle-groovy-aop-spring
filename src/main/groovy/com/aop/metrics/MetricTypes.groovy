package com.aop.metrics

enum MetricTypes{
    //HttpTimeout,HttpNotRespond
    OblikRestAccess ("OBLIK_REST_ACCESS"),
    ParsivRestAccess ("PARSIV_REST_ACCESS"),
    DbAccess ("DB_ACCESS"),
    CacheAccess  ("CACHE_ACCESS"),
    BrokerPublish  ("BROKER_ACCESS")

    MetricTypes(String val){
        this.value = val;
    }
    private final String value
    String value() { return value }
}